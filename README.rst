ylib
================================================================================

[![PyPI](https://img.shields.io/pypi/v/ilib.py)](https://pypi.org/project/ilib.py/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/ilib.py)](https://pypi.org/project/ilib.py/)
[![pipeline status](https://gitlab.com/yassu/ilib.py/badges/master/pipeline.svg)](https://gitlab.com/yassu/ilib.py/-/pipelines/latest)
[![coverage report](https://gitlab.com/yassu/ilib.py/badges/master/coverage.svg)](https://gitlab.com/yassu/ilib.py/-/commits/master)
[![PyPI - License](https://img.shields.io/pypi/l/ilib.py)](https://gitlab.com/yassu/ilib.py/-/raw/master/LICENSE)

How to Install
=======================

::

    $ poetry init
    $ poetry install
    $ poetry run python setup.py develop
