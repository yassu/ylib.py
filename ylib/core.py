#!/usr/bin/env python
# -*- coding: utf-8 -*-


def parse_triple_string(s: str, start_char: str = '|') -> str:
    r'''
    >>> parse_triple_string("|abc")
    'abc'
    >>> parse_triple_string("|abc\n    |bcd\n|cde\n")
    'abc\nbcd\ncde'
    >>> parse_triple_string("@abc\n    @bcd\n@cde\n", '@')
    'abc\nbcd\ncde'
    '''
    target_lines = s.split('\n')
    if target_lines[0] == '':
        target_lines = target_lines[1:]

    if target_lines[-1] == '':
        target_lines = target_lines[:-1]

    lines = []
    for line in target_lines:
        lines.append(line[line.index(start_char) + 1:])

    return '\n'.join(lines)


triple_str = parse_triple_string


def include_poses(iter_, cond_f):
    """
    >>> list(include_poses(range(-1, 10), lambda x: x % 3 == 0))
    [1, 4, 7, 10]
    """
    for i, x in enumerate(iter_):
        if cond_f(x):
            yield i


def include_items(iter_, cond_f):
    """
    >>> list(include_items(range(-1, 10), lambda x: x % 3 == 0))
    [0, 3, 6, 9]
    """
    for x in iter_:
        if cond_f(x):
            yield x


def includes(iter_, cond_f):
    """
    >>> list(includes(range(-1, 10), lambda x: x % 3 == 0))
    [(1, 0), (4, 3), (7, 6), (10, 9)]
    """
    for i, x in enumerate(iter_):
        if cond_f(x):
            yield i, x
