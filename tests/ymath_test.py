#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ylib.ymath import fib


def test_fib():
    assert fib(100) == 354224848179261915075
