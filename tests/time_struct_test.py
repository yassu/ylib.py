#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from ylib.time_struct import TimeStruct, _time_type


@pytest.mark.parametrize(
    "h, m, s, expected", [
        (0, 0, 0, (0, 0, 0)), (1, 2.0, 3, (1.0, 2.0, 3.0)), (1.0, 2.0, 3.0, (1.0, 2.0, 3.0)),
        (0.0, 0.0, 0.0, (0.0, 0.0, 0.0)), (1, 2, 3, (1, 2, 3)), (3, 122, 181, (5, 5, 1))
    ])
def test_TimeStruct_regal(h, m, s, expected):
    t = TimeStruct(h, m, s)
    assert (t.tm_hour, t.tm_min, t.tm_sec) == expected


def test_TimeStruct_add():
    assert TimeStruct(1, 2, 3) + TimeStruct(2, 3, 5) == TimeStruct(3, 5, 8)


def test_TimeStruct_sub():
    assert TimeStruct(1, 2, 3) - TimeStruct(2, 3, 5) == TimeStruct(-1, -1, -2)


def test_TimeStruct_mul():
    assert TimeStruct(2, 3, 5) * 3 == TimeStruct(6, 9, 15)


def test_TimeStruct_truediv():
    assert TimeStruct(2, 4, 6) / 2 == TimeStruct(1.0, 2.0, 3.0)


@pytest.mark.parametrize(
    "h, m, s, expected",
    [(0, 0, 0, int), (1, 2.0, 3, float), (1.0, 2.0, 3.0, float), (0.0, 0.0, 0.0, float)])
def test_time_type(h, m, s, expected):
    assert _time_type(h, m, s) is expected
